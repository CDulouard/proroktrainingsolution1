﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Ressources.Scripts
{
    public class Config : MonoBehaviour
    {
        private static readonly string ControlPath = Application.streamingAssetsPath + "/Configuration/control.json";
        private static readonly string ServerPath = Application.streamingAssetsPath + "/Configuration/server.json";
        
        public static Dictionary<string, Dictionary<string, string>> Configuration;

        public static List<List<char>> CurrentMap;
        public static List<GameObject> PlayerList;

        public static KeyCode KcRotateLeft;
        public static KeyCode KcRotateRight;

        public static KeyCode KcUp;
        public static KeyCode KcDown;
        public static KeyCode KcLeft;
        public static KeyCode KcRight;

        private void Awake()
        {
            Configuration = new Dictionary<string, Dictionary<string, string>>();
            PlayerList = new List<GameObject>();
            RefreshControl();
            RefreshServer();
        }


        private static void LoadSettings(string path, string configName)
        {
            var jsonString = File.ReadAllText(path);
            try
            {
                Configuration.Add(configName, JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString));
            }
            catch (Exception)
            {
                Configuration[configName] = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString);
            }
        }

        private static void RefreshServer()
        {
            LoadSettings(ServerPath, "server");
        }

        private static void RefreshControl()
        {
            LoadSettings(ControlPath, "control");
            KcRotateLeft = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["rotateLeft"]);
            KcRotateRight = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["rotateRight"]);

            KcUp = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["up"]);
            KcDown = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["down"]);
            KcLeft = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["left"]);
            KcRight = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["right"]);
        }

        public static void SetCurrentMap(List<List<char>> newMap)
        {
            CurrentMap = newMap;
        }

        public static void AddPlayer(GameObject player)
        {
            PlayerList.Add(player);
        }
    }
}