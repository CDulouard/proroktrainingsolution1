﻿using UnityEngine;

namespace Ressources.Scripts
{
    public class UI_BoxManager : MonoBehaviour
    {
        public void ChangeState()
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(!child.gameObject.activeSelf);
            }
        }
    }
}