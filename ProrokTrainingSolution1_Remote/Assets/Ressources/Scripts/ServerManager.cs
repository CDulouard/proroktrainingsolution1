﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Button = UnityEngine.UIElements.Button;

namespace Ressources.Scripts
{
    public class ServerManager : MonoBehaviour
    {
        public string ipAddressServer = "127.0.0.1";
        public int portServer = 50000;
        public string ipAddressClient = "127.0.0.1";
        public int portClient = 50001;
        public string password = "";
        public int bufferSize = 8192;
        public long delayCheck;
        public long delayDisconnection;
        public float refreshTime;

        public InputField ipClientField;
        public InputField ipServerField;
        public InputField portClientField;
        public InputField portServerField;
        public InputField passwordField;

        public Text serverStatus;


        private const int PortMin = 1000;
        private const int PortMax = 100000;
        private long _lastCheck;
        private float _timeLastRefresh = 0f;

        public UdpSocket server;

        public int playerNo = 1;
        
        public bool moveUp = false;
        public bool moveDown = false;
        public bool moveLeft = false;
        public bool moveRight = false;

        public bool rotateLeft = false;
        public bool rotateRight = false;

        public Button moveUpButton;

        // Start is called before the first frame update
        private void Start()
        {
            server = new UdpSocket();
            ipAddressClient = Config.Configuration["server"]["ip"];
            ipAddressServer = Config.Configuration["server"]["serverIp"];
            portClient = int.Parse(Config.Configuration["server"]["port"]);
            portServer = int.Parse(Config.Configuration["server"]["serverPort"]);
            password = Config.Configuration["server"]["password"];
            bufferSize = int.Parse(Config.Configuration["server"]["bufferSize"]);
            delayCheck = long.Parse(Config.Configuration["server"]["delayCheckConnection"]);
            delayDisconnection = long.Parse(Config.Configuration["server"]["delayDisconnection"]);
            refreshTime = float.Parse(Config.Configuration["server"]["refreshTime"]);

            ipClientField.text = ipAddressClient;
            ipServerField.text = ipAddressServer;
            portClientField.text = portClient.ToString();
            portServerField.text = portServer.ToString();
            passwordField.text = password;

            _lastCheck = 0;

            serverStatus.text = "Disconnected";
            serverStatus.color = Color.red;
        }

        public void StartServer()
        {
            if (CheckIp(ipAddressClient) && CheckPort(portClient))
            {
                server.Start(ipAddressClient, portClient, password, bufferSize);
            }
        }

        private void Update()
        {
            if (CheckServerIsConnected())
            {
                serverStatus.text = "Connected";
                serverStatus.color = Color.green;

                if (_timeLastRefresh >= refreshTime)
                {
                    server.SendTo(ipAddressServer, portServer, Message.CreatePlayerCommandMessage(playerNo, moveUp,
                        moveDown, moveLeft, moveRight, rotateLeft, rotateRight).ToJson());
                    _timeLastRefresh = 0f;
                }
                else
                {
                    _timeLastRefresh += Time.deltaTime;
                }
            }
            else
            {
                serverStatus.text = "Disconnected";
                serverStatus.color = Color.red;
            }
        }
        

        public bool CheckIp(string ip)
        {
            // TODO : Write code that check ip
            return true;
        }

        public bool CheckPort(int port)
        {
            if (port >= PortMin && port <= PortMax)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CheckPortClientField()
        {
            var testPort = int.Parse(portClientField.text);
            if (CheckPort(testPort))
            {
                portClient = testPort;
            }
            else
            {
                portClientField.text = portClient.ToString();
            }
        }

        public void CheckPortServerField()
        {
            var testPort = int.Parse(portServerField.text);
            if (CheckPort(testPort))
            {
                portServer = testPort;
            }
            else
            {
                portServerField.text = portServer.ToString();
            }
        }

        public void CheckIpClientField()
        {
            if (CheckIp(ipClientField.text))
            {
                ipAddressClient = ipClientField.text;
            }
            else
            {
                ipClientField.text = ipAddressClient;
            }
        }

        public void CheckIpServerField()
        {
            if (CheckIp(ipServerField.text))
            {
                ipAddressServer = ipServerField.text;
            }
            else
            {
                ipServerField.text = ipAddressServer;
            }
        }

        public bool CheckServerIsRunning()
        {
            return server.IsActive;
        }

        public bool CheckServerIsConnected()
        {
            var isConnected = false;
            if (CheckServerIsRunning() && CheckIp(ipAddressServer) && CheckPort(portServer) &&
                DateTime.Now.Ticks - _lastCheck > delayCheck)
            {
                _lastCheck = DateTime.Now.Ticks;
                server.SendTo(ipAddressServer, portServer, "check");
            }


            if (CheckServerIsRunning() && CheckIp(ipAddressServer) && CheckPort(portServer) &&
                server.GetLastCheckEndPoint() != null)
            {
                if (server.GetLastCheckEndPoint().Address.ToString() == ipAddressServer &&
                    server.GetLastCheckEndPoint().Port == portServer &&
                    DateTime.Now.Ticks - server.GetLastCheckTime() < delayDisconnection)
                {
                    isConnected = true;
                }
            }

            return isConnected;
        }
        
        //=========================================
        // BUTTON STUFFS
        //=========================================
        public void MoveUp()
        {
            moveUp = true;
        }

        public void StopMoveUp()
        {
            moveUp = false;
        }
        
        
        public void MoveDown()
        {
            moveDown = true;
        }

        public void StopMoveDown()
        {
            moveDown = false;
        }
        
        public void MoveLeft()
        {
            moveLeft = true;
        }

        public void StopMoveLeft()
        {
            moveLeft = false;
        }
        
        
        public void MoveRight()
        {
            moveRight = true;
        }

        public void StopMoveRight()
        {
            moveRight = false;
        }
        
        public void RotateLeft()
        {
            rotateLeft = true;
        }

        public void StopRotateLeft()     
        {
            rotateLeft = false;
        }
        
        public void RotateRight()
        {
            rotateRight = true;
        }

        public void StopRotateRight()
        {
            rotateRight = false;
        }
    }
}