from Game import Game
from typing import Dict, Optional
from datetime import datetime
import time


def will_colliding(sensors_data: Dict[str, int], threshold: Optional[int] = 1):
    max_val = 0
    for key in ["sensorFront", "sensorFrontLeft", "sensorFrontRight"]:
        if key in sensors_data.keys():
            if sensors_data[key] > max_val:
                max_val = sensors_data[key]
    return True if max_val > threshold else False


class Demo:

    def __init__(self, game: Game, duration_s: Optional[int] = 30):
        self.game = game
        t_start = datetime.now()
        while (datetime.now() - t_start).total_seconds() < duration_s:
            for player_id in game.get_players_id():
                sensors_data = game.get_sensors_data(player_id)
                if will_colliding(sensors_data):
                    self.chose_new_direction(sensors_data, player_id)
                    time.sleep(0.001)
                    self.game.command_player(player_id)
                else:
                    game.command_player(player_id, move_up=True)

    def chose_new_direction(self, sensors_data: Dict[str, int], player_id: int, threshold: Optional[int] = 20):
        if will_colliding(sensors_data):
            if sensors_data["sensorFront"] > threshold:
                direction = True
                # direction = True if sensors_data["sensorFrontLeft"] >= sensors_data["sensorFrontRight"] else False
                self.game.command_player(player_id, rotate_right=True if direction else False,
                                         rotate_left=True if not direction else False)
            else:
                self.game.command_player(player_id, move_up=True)
