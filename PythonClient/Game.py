from UdpSocket import UdpSocket
import datetime
from Message import Message
import time
from threading import Thread
from typing import Optional, List, Dict, Tuple, Union
import json
from PlayerInfo import PlayerInfo


class Game(Thread):

    def __init__(self, server_ip: str, server_port: int, socket_ip: str, socket_port: int,
                 password: Optional[str] = "test", buffer_size: Optional[int] = 16384,
                 delay_check_connection: Optional[int] = 1000, delay_disconnection: Optional[int] = 3000,
                 min_refresh_time: Optional[int] = 50) -> None:

        #  Server
        self.delay_check_connection = delay_check_connection
        self.delay_disconnection = delay_disconnection
        self.server_ep = (server_ip, server_port)
        self.last_check_send = datetime.datetime.now()
        Thread.__init__(self)
        self.socket = UdpSocket(buffer_size)
        self.socket.start_socket(socket_ip, socket_port, password)
        self.is_connected = False

        #  Game
        self.min_refresh_time = min_refresh_time
        self.players: Dict[int, PlayerInfo] = {}
        self.players_commands: Dict[int] = {}
        self.last_refresh = datetime.datetime.now()

        #  Start instance
        self.stop_instance = False
        self.start()

    def check_connection(self) -> None:
        """
        Check if server is responding every self.delay_check_connection ms. If the server reply, self.is_connected is
        set to True. If last reply time exceed self.delay_disconnection, self.is_connected is set to False
        :return: None
        """
        if self.socket.time_since_last_check() > self.delay_check_connection:
            self.socket.check(self.server_ep)
        if self.socket.time_since_last_check() > self.delay_disconnection and self.is_connected:
            self.is_connected = False
        if not self.is_connected:
            if self.socket.time_since_last_check() < self.delay_disconnection and \
                    self.socket.last_check_ep == self.server_ep:
                self.is_connected = True

    def run(self) -> None:
        """
        Start the Thread
        :return: None
        """
        while not self.stop_instance:

            if self.is_connected:
                self.refresh_information()
                if not self.socket.queue.empty():
                    self.handler(self.socket.queue.get_nowait())
                self.update()

            self.check_connection()
            time.sleep(0.001)

    def stop(self) -> None:
        """
        Stop the socket and the Thread
        :return: None
        """
        self.socket.stop_socket()
        self.stop_instance = True
        self.join()

    def update(self) -> None:
        """
        Function executed in the Thread loop
        :return: None
        """
        pass

    def send_information(self) -> None:
        """
        Send information to the game
        :return: None
        """
        self.send_commands()

    def get_information(self) -> None:
        """
        Get information from the game
        :return: None
        """
        self.socket.send_to(self.server_ep, Message(301, "").to_json())

    def refresh_information(self) -> None:
        """
        Refresh information of the game (send local information and get game's information
        :return: None
        """
        if (datetime.datetime.now() - self.last_refresh).total_seconds() * 1_000 > self.min_refresh_time:
            self.last_refresh = datetime.datetime.now()
            self.get_information()
            self.send_information()

    def handler(self, msg: Message) -> None:
        """
        Manage the received messages
        :param msg: Message object received by the socket
        :return: None
        """
        if msg.id == 202:
            rcv_dict = json.loads(msg.message)
            self.players = {}
            for key in rcv_dict:
                self.players[int(key)] = PlayerInfo.from_json(rcv_dict[key])
            # print(self.players[1].pos_x, self.players[1].pos_y, self.players[1].rotation)
            # print(self.players[1].sensors_data["sensorBack"])

    def command_player(self, player_no: int = 1, move_up: bool = False, move_down: bool = False,
                       move_left: bool = False,
                       move_right: bool = False, rotate_left: bool = False, rotate_right: bool = False) -> None:
        """
        Set the commands to send for a given player
        :param player_no: The id of the player the command is for
        :param player_no: The id of the player to command
        :param move_up: True if the player must go up
        :param move_down: True if the player must go down
        :param move_left: True if the player must go left
        :param move_right: True if the player must go right
        :param rotate_left: True if the player must rotate left
        :param rotate_right: True if the player must rotate right
        :return: None
        """
        if player_no in self.players.keys():
            self.players_commands[player_no] = {"move_up": move_up, "move_down": move_down, "move_left": move_left,
                                                "move_right": move_right, "rotate_left": rotate_left,
                                                "rotate_right": rotate_right}

    def send_commands(self) -> None:
        """
        Send the players commands to the server
        :return: None
        """
        for key in self.players.keys():
            if key in self.players_commands.keys():
                self.socket.send_to(self.server_ep, Message.command_message(key, self.players_commands[key]["move_up"],
                                                                            self.players_commands[key]["move_down"],
                                                                            self.players_commands[key]["move_left"],
                                                                            self.players_commands[key]["move_right"],
                                                                            self.players_commands[key]["rotate_left"],
                                                                            self.players_commands[key][
                                                                                "rotate_right"]))

    def get_position(self, player_id: int) -> Union[Tuple[float, float, float], None]:
        """
        Function used to get player's position and rotation. Return posX, posY and rotation if the player exists else
        return None.
        :return: posX: float, posY: float and rotation: float if the given id is correct else None.
        """
        if player_id in self.players.keys():
            return self.players[player_id].pos_x, self.players[player_id].pos_y, self.players[player_id].rotation
        else:
            return None

    def get_camera_lbl(self, player_id: int) -> Union[List[str], None]:
        """
        Function used to get player's camera labels. Return camera labels if given id exists else None.
        :return: camera_lbl: List[str] if given id exists else None.
        """
        if player_id in self.players.keys():
            return self.players[player_id].camera_lbl
        else:
            return None

    def get_camera_pxl_distances(self, player_id: int) -> Union[List[int], None]:
        """
        Function used to get player's camera pixels distances. Return camera pixels distances if given id exists
        else None.
        :return: camera_pxl_distance: List[int] if given id exists else None.
        """
        if player_id in self.players.keys():
            return self.players[player_id].camera_pxl_distance
        else:
            return None

    def get_sensors_data(self, player_id: int) -> Union[Dict[str, int], None]:
        """
        Function used to get player's sensors data. Return player's sensors data if given id exists else None.
        :return: sensors_data: Dict[str, int] if given id exists else None.
        """
        if player_id in self.players.keys():
            return self.players[player_id].sensors_data
        else:
            return None

    def get_players_id(self) -> List[int]:
        """
        Returns all available player's ids
        :return: players_id : List[int] that contains all available players ids.
        """
        return list(self.players.keys())
