import json
from typing import List, Dict


class PlayerInfo:

    def __init__(self, pos_x: float, pos_y: float, rotation: float, camera_lbl: List[str],
                 camera_pxl_distance: List[int], sensors_data: Dict[str, int]):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.rotation = rotation
        self.camera_lbl = camera_lbl
        self.camera_pxl_distance = camera_pxl_distance
        self.sensors_data = sensors_data

    @staticmethod
    def from_json(json_str: str):
        temp = json.loads(json_str)
        pos_x = float(temp["posX"]) if "posX" in temp.keys() else 0
        pos_y = float(temp["posY"]) if "posY" in temp.keys() else 0
        rotation = float(temp["rotation"]) if "rotation" in temp.keys() else 0
        camera_lbl = list(temp["cameraLbl"]) if "cameraLbl" in temp.keys() else []
        camera_pxl_distance = json.loads(temp["cameraPxlDistance"]) if "cameraPxlDistance" in temp.keys() else []
        sensors_data = json.loads(temp["sensorsData"]) if "sensorsData" in temp.keys() else {}
        return PlayerInfo(pos_x, pos_y, rotation, camera_lbl, camera_pxl_distance, sensors_data)
