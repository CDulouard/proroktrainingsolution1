﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;


namespace Ressources.Scripts
{
    public class PlayerCamera : MonoBehaviour
    {
        // TODO : Rebuild this class to make it work in a most efficient way
        public Transform cameraAnchor;

        private float _cameraFar = 5f;
        private float _cameraAngle = 30f;
        private int _cameraResolution = 480;

        private List<string> _imgLbl;
        private List<int> _pixelsDistances;
        private string _imgLblStr = "";
        private string _pixelsDistancesStr = "";

        // private bool _debugBool = false;

        private void Awake()
        {
            _imgLbl = new List<string>();
            _pixelsDistances = new List<int>();
        }

        private void Start()
        {
            RefreshCameraProperties();
        }


        private void RefreshCameraProperties()
        {
            _cameraFar = float.Parse(Config.Configuration["physic"]["playerCameraFar"]);
            _cameraAngle = float.Parse(Config.Configuration["physic"]["playerCameraAngle"]);
            _cameraResolution = int.Parse(Config.Configuration["physic"]["playerCameraResolution"]);
        }

        public void RefreshCam()
        {
            // TODO : Check if camera is working well

            var dAngle = _cameraAngle / _cameraResolution;
            var position = cameraAnchor.transform.position;
            var up = transform.up;

            var tmpPixelDistance = new List<int>();
            var tmpImgLbl = new List<string>();

            // var tStart = DateTime.Now; // DEBUG
            for (var i = 0; i < _cameraResolution; i++)
            {
                var direction = Quaternion.Euler(0, 0, _cameraAngle / 2 - i * dAngle) * up;
                var hit = Physics2D.Raycast(position, direction * _cameraFar);
                // Debug.DrawRay(position, direction * hit.distance, Color.green);

                var temp = hit.rigidbody;
                if (temp != null)
                {
                    tmpImgLbl.Add(temp.GetComponent<Text>().text);
                    tmpPixelDistance.Add((int) (hit.distance * 100f));
                }
                else
                {
                    tmpImgLbl.Add("_");
                    tmpPixelDistance.Add((int) (_cameraFar * 100f));
                }
            }

            // Debug.Log("===CAMERA BLOCKED=== : " + (DateTime.Now - tStart).TotalMilliseconds);

            lock (_imgLbl)
            {
                _imgLbl = tmpImgLbl;
            }

            lock (_pixelsDistances)
            {
                _pixelsDistances = tmpPixelDistance;
            }
        }

        public void RefreshCameraLblJson()
        {
            List<string> tempImgLbl;
            lock (_imgLbl)
            {
                tempImgLbl = _imgLbl.ConvertAll(val => val);
            }

            // DEBUG

            var tmpStr = "";

            var batchSize = 300;
            var totalLen = tempImgLbl.Count;
            var nbFullBatch = totalLen / batchSize;

            for (var j = 0; j < nbFullBatch; j++)
            {
                for (var k = j * batchSize; k < j * batchSize + batchSize; k++)
                {
                    tmpStr += string.Copy(tempImgLbl[k]);
                }

                Thread.Sleep(1);
            }

            for (var k = nbFullBatch * batchSize; k < totalLen; k++)
            {
                tmpStr += string.Copy(tempImgLbl[k]);
            }

            lock (_imgLblStr)
            {
                _imgLblStr = tmpStr;
            }
        }

        public void RefreshCameraDistanceJson()
        {
            List<int> tmpPixelDistances;
            lock (_pixelsDistances)
            {
                tmpPixelDistances = _pixelsDistances.ConvertAll(val => val);
            }

            var tempPixelDistanceStr = "[";

            if (tmpPixelDistances.Count == 0) return;

            for (var i = 0; i < tmpPixelDistances.Count - 1; i++)
            {
                tempPixelDistanceStr += tmpPixelDistances[i] + ",";
            }

            tempPixelDistanceStr += tmpPixelDistances[tmpPixelDistances.Count - 1] + "]";

            lock (_pixelsDistancesStr)
            {
                _pixelsDistancesStr = tempPixelDistanceStr;
            }
        }

        public string GetCameraLblJson()
        {
            lock (_imgLblStr)
            {
                return _imgLblStr;
            }
        }

        public string GetCameraPxlJson()
        {
            lock (_pixelsDistancesStr)
            {
                return _pixelsDistancesStr;
            }
        }
    }
}