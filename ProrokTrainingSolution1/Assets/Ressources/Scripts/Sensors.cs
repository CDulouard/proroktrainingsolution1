﻿using System.Collections.Generic;
using UnityEngine;

namespace Ressources.Scripts
{
    public class Sensors : MonoBehaviour
    {
        public Transform sensorFront;
        public Transform sensorBack;
        public Transform sensorFrontLeft;
        public Transform sensorFrontRight;
        public Transform sensorBackLeft;
        public Transform sensorBackRight;

        private float _sensorFar;
        private float _sensorAngle;
        private int _sensorNbRay;

        private Dictionary<string, int> _sensorsData;

        // Start is called before the first frame update
        private void Awake()
        {
            _sensorFar = float.Parse(Config.Configuration["physic"]["sensorFar"]);
            _sensorAngle = float.Parse(Config.Configuration["physic"]["sensorAngle"]);
            _sensorNbRay = int.Parse(Config.Configuration["physic"]["sensorNbRay"]);

            // ReSharper disable once InconsistentlySynchronizedField
            _sensorsData = new Dictionary<string, int>
            {
                {"sensorFront", 100},
                {"sensorBack", 100},
                {"sensorFrontLeft", 100},
                {"sensorFrontRight", 100},
                {"sensorBackLeft", 100},
                {"sensorBackRight", 100}
            };
        }

        // Update is called once per frame
        private void Update()
        {
            RefreshSensorsData();
        }

        private void RefreshSensorsData()
        {
            lock (_sensorsData)
            {
                _sensorsData["sensorFront"] = GetSensorData(sensorFront);
                _sensorsData["sensorBack"] = GetSensorData(sensorBack);
                _sensorsData["sensorFrontLeft"] = GetSensorData(sensorFrontLeft);
                _sensorsData["sensorFrontRight"] = GetSensorData(sensorFrontRight);
                _sensorsData["sensorBackLeft"] = GetSensorData(sensorBackLeft);
                _sensorsData["sensorBackRight"] = GetSensorData(sensorBackRight);
            }
        }

        private int GetSensorData(Transform sensor)
        {
            var distance = _sensorFar;
            var position = sensor.position;


            var dAngle = _sensorAngle / _sensorNbRay;
            for (var i = 0; i < _sensorNbRay; i++)
            {
                var direction = Quaternion.Euler(0, 0, _sensorAngle / 2 - i * dAngle) * sensor.up;
                var hit = Physics2D.Raycast(position, direction * _sensorFar);
                if (hit.distance < distance)
                {
                    distance = hit.distance;
                }
            }

            return -(int) ((distance * 100 / _sensorFar) - 100);
        }

        public Dictionary<string, int> GetSensorsData()
        {
            var toReturn = new Dictionary<string, int>();
            lock (_sensorsData)
            {
                foreach (var element in _sensorsData)
                {
                    toReturn.Add(element.Key, element.Value);
                }

                return toReturn;
            }
        }
    }
}