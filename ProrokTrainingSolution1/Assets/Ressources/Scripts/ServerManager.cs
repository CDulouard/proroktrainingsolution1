﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Ressources.Scripts
{
    public class ServerManager : MonoBehaviour
    {
        public string ipAddressServer = "127.0.0.1";
        public int portServer = 50000;
        public string password = "";
        public int bufferSize = 8192;

        public InputField ipField;
        public InputField portField;
        public InputField passwordField;

        public Text serverStatus;


        private const int PortMin = 1000;
        private const int PortMax = 100000;

        public UdpSocket server;

        // Start is called before the first frame update
        private void Start()
        {
            server = new UdpSocket();
            ipAddressServer = Config.Configuration["server"]["ip"];
            portServer = int.Parse(Config.Configuration["server"]["port"]);
            password = Config.Configuration["server"]["password"];
            bufferSize = int.Parse(Config.Configuration["server"]["bufferSize"]);

            ipField.text = ipAddressServer;
            portField.text = portServer.ToString();
            passwordField.text = password;

            serverStatus.text = "Stopped";
            serverStatus.color = Color.red;
        }

        public void StartServer()
        {
            server = new UdpSocket();
            if (CheckIp(ipAddressServer) && CheckPort(portServer))
            {
                server.Start(ipAddressServer, portServer, password, bufferSize);
                if (CheckServerIsRunning())
                {
                    serverStatus.text = "Started";
                    serverStatus.color = Color.green;
                }
            }
        }


        public bool CheckIp(string ip)
        {
            // TODO : Write code that check ip
            return true;
        }

        public bool CheckPort(int port)
        {
            if (port >= PortMin && port <= PortMax)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CheckPortField()
        {
            var testPort = int.Parse(portField.text);
            if (CheckPort(testPort))
            {
                portServer = testPort;
            }
            else
            {
                portField.text = portServer.ToString();
            }
        }

        public void CheckIpField()
        {
            if (CheckIp(ipField.text))
            {
                ipAddressServer = ipField.text;
            }
            else
            {
                ipField.text = ipAddressServer;
            }
        }

        private bool CheckServerIsRunning()
        {
            return server.IsActive;
        }
    }
}