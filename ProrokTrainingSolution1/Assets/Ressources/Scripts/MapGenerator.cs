﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using UnityEngine.Serialization;

namespace Ressources.Scripts
{
    public class MapGenerator : MonoBehaviour
    {
        private const float SpriteLen = .5f;
        private const int MaxMapSize = 2048;

        public string mapName;
        public GameObject player;

        public GameObject unknown;
        public GameObject ground;
        public GameObject wall;

        private readonly List<char> _specialGroundCells = new List<char>() {'S'};

        // Start is called before the first frame update
        private void Start()
        {
            CreateMap();
        }

        private void CreateMap()
        {
            var map = new List<List<char>> {new List<char>()};
            var mapPath = Application.streamingAssetsPath + "/Map/" + mapName + ".map";
            var rawMap = File.ReadAllText(mapPath);

            var cursor = new Vector3(0, 0, 0);

            var mapX = 0;
            var mapY = 0;

            foreach (var cell in rawMap)
            {
                var cellToAdd = cell;
                switch (cell)
                {
                    case '0':
                        Instantiate(ground, cursor, Quaternion.identity);
                        break;
                    case 'p':
                        Instantiate(ground, cursor, Quaternion.identity);
                        Config.AddPlayer(Instantiate(player, cursor, Quaternion.identity));
                        break;
                    case 'x':
                        Instantiate(wall, cursor, Quaternion.identity);
                        break;
                    case '\n':
                        cursor.x = -SpriteLen;
                        cursor.y -= SpriteLen;
                        map.Add(new List<char>());
                        mapY += 1;
                        break;
                    case ' ':
                        break;
                    default:
                        if (cell != new byte() && cell != '\r')
                        {
                            Instantiate(unknown, cursor, Quaternion.identity);
                        }

                        break;
                }

                if (_specialGroundCells.Contains(cell))
                {
                    cellToAdd = '0';
                }

                cursor.x += SpriteLen;
                if (cellToAdd != new byte() && cellToAdd != '\r' && cellToAdd != '\n')
                {
                    map[mapY].Add(cellToAdd);
                    mapX += 1;
                }
            }
            Config.SetCurrentMap(map);
        }
    }
}