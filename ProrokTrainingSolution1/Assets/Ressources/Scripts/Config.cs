﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UIElements;

namespace Ressources.Scripts
{
    public class Config : MonoBehaviour
    {
        private static readonly string ControlPath = Application.streamingAssetsPath + "/Configuration/control.json";
        private static readonly string PhysicPath = Application.streamingAssetsPath + "/Configuration/physic.json";
        private static readonly string ServerPath = Application.streamingAssetsPath + "/Configuration/server.json";

        public static Dictionary<string, Dictionary<string, string>> Configuration;

        public static List<List<char>> CurrentMap;
        public static List<GameObject> PlayerList;
        public static List<PlayerBehaviour> PlayerBehaviourList;
        public static List<Transform> PlayerTransformList;
        public static List<Vector3> PlayerPositionList;
        public static List<Quaternion> PlayerRotationList;

        public static KeyCode KcRotateLeft;
        public static KeyCode KcRotateRight;

        public static KeyCode KcUp;
        public static KeyCode KcDown;
        public static KeyCode KcLeft;
        public static KeyCode KcRight;

        public static float RefreshDelay;

        private double _lastRefreshThread;
        private double _lastRefreshThreadMs;

        private float _lastRefresh;

        private Thread _refreshThread;
        private bool _mustRefresh = true;

        private bool _debugBool = false;

        private void Awake()
        {
            _lastRefreshThread = 100000f;
            _lastRefresh = 100000f;
            _lastRefreshThreadMs = DateTime.Now.Ticks / (double) TimeSpan.TicksPerMillisecond;
            Configuration = new Dictionary<string, Dictionary<string, string>>();

            // ReSharper disable once InconsistentlySynchronizedField
            PlayerList = new List<GameObject>();
            // ReSharper disable once InconsistentlySynchronizedField
            PlayerBehaviourList = new List<PlayerBehaviour>();
            PlayerTransformList = new List<Transform>();
            PlayerPositionList = new List<Vector3>();
            PlayerRotationList = new List<Quaternion>();

            RefreshControl();
            RefreshPhysic();
            RefreshServer();
        }

        private void Start()
        {
            _refreshThread = new Thread(RefreshThread);
            _refreshThread.IsBackground = true;
            _refreshThread.Start();
        }

        private void Update()
        {
            Refresh();
        }

        private void OnApplicationQuit()
        {
            _mustRefresh = false;
        }

        private void Refresh()
        {
            if (_lastRefresh > RefreshDelay)
            {
                List<Transform> tmpPlayerTransformList;
                lock (PlayerTransformList)
                {
                    tmpPlayerTransformList = PlayerTransformList;
                }

                var tmpPositionList = new List<Vector3>();
                var tmpRotationList = new List<Quaternion>();


                for (var playerId = 0; playerId < tmpPlayerTransformList.Count; playerId++)
                {
                    tmpPositionList.Add(tmpPlayerTransformList[playerId].position);
                    tmpRotationList.Add(tmpPlayerTransformList[playerId].rotation);
                    lock (PlayerBehaviourList[playerId])
                    {
                        PlayerBehaviourList[playerId].RefreshCamera();
                    }
                }

                lock (PlayerPositionList)
                {
                    PlayerPositionList = tmpPositionList;
                }

                lock (PlayerRotationList)
                {
                    PlayerRotationList = tmpRotationList;
                }


                _lastRefresh = 0f;
            }
            else
            {
                _lastRefresh += Time.deltaTime;
            }
        }


        private void RefreshThread()
        {
            while (_mustRefresh)
            {
                if (_lastRefreshThread > RefreshDelay)
                {
                    var newInfo = GetPlayersInformation();

                    List<PlayerBehaviour> tempPlayerBehaviourList;
                    lock (PlayerBehaviourList)
                    {
                        tempPlayerBehaviourList = PlayerBehaviourList;
                    }

                    var nPlayer = tempPlayerBehaviourList.Count;

                    for (var playerId = 0; playerId < nPlayer; playerId++)
                    {
                        tempPlayerBehaviourList[playerId].RefreshCameraAsync();
                    }

                    // if (_debugBool) continue;

                    UdpSocket.SetPlayerInformation(new Message(202, newInfo).ToJson());
                    // Debug.Log(GetPlayersInformation());
                    _lastRefreshThread = 0;
                    // _debugBool = true;
                }
                else
                {
                    _lastRefreshThread += DateTime.Now.Ticks / (double) TimeSpan.TicksPerMillisecond -
                                          _lastRefreshThreadMs;
                }

                //TODO : Improve function efficiency by setting the correct amount of time in thread sleep
                Thread.Sleep(1);
            }
        }


        private static void LoadSettings(string path, string configName)
        {
            var jsonString = File.ReadAllText(path);
            try
            {
                Configuration.Add(configName, JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString));
            }
            catch (Exception)
            {
                Configuration[configName] = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString);
            }
        }

        private static void RefreshServer()
        {
            LoadSettings(ServerPath, "server");
        }

        private static void RefreshPhysic()
        {
            LoadSettings(PhysicPath, "physic");
            RefreshDelay = float.Parse(Configuration["physic"]["refreshDelay"]) / 1000f;
        }

        private static void RefreshControl()
        {
            LoadSettings(ControlPath, "control");
            KcRotateLeft = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["rotateLeft"]);
            KcRotateRight = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["rotateRight"]);

            KcUp = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["up"]);
            KcDown = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["down"]);
            KcLeft = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["left"]);
            KcRight = (KeyCode) System.Enum.Parse(typeof(KeyCode), Configuration["control"]["right"]);
        }

        public static void SetCurrentMap(List<List<char>> newMap)
        {
            CurrentMap = newMap;
        }

        public static void AddPlayer(GameObject player)
        {
            lock (PlayerList)
            {
                PlayerList.Add(player);
            }

            lock (PlayerBehaviourList)
            lock (PlayerTransformList)
            {
                PlayerBehaviourList.Add(player.GetComponent<PlayerBehaviour>());
                PlayerTransformList.Add(player.transform);
                PlayerPositionList.Add(player.transform.position);
                PlayerRotationList.Add(player.transform.rotation);
            }
        }

        public static GameObject GetPlayer(int id)
        {
            lock (PlayerList)
            {
                return id < PlayerList.Count && id >= 0 ? PlayerList[id] : null;
            }
        }

        public static PlayerBehaviour GetPlayerBehaviour(int id)
        {
            lock (PlayerBehaviourList)
            {
                return id < PlayerBehaviourList.Count && id >= 0 ? PlayerBehaviourList[id] : null;
            }
        }

        private static string GetPlayersInformation()
        {
            lock (PlayerList)
            {
                var dictToReturn = new Dictionary<string, string>();
                for (var i = 0; i < PlayerList.Count; i++)
                {
                    dictToReturn.Add((i + 1).ToString(), GetPlayerInformation(i));
                }

                return JsonConvert.SerializeObject(dictToReturn);
            }
        }

        private static string GetPlayerInformation(int playerId)
        {
            lock (PlayerList)
            {
                if (playerId >= PlayerList.Count) return "";
                var dictToReturn = new Dictionary<string, string>();
                var playerTransform = PlayerTransformList[playerId];
                var position = PlayerPositionList[playerId];
                dictToReturn.Add("posX", position.x.ToString(CultureInfo.InvariantCulture));
                dictToReturn.Add("posY", position.y.ToString(CultureInfo.InvariantCulture));
                dictToReturn.Add("rotation",
                    (PlayerRotationList[playerId].eulerAngles.z).ToString(CultureInfo.InvariantCulture));
                dictToReturn.Add("cameraLbl", PlayerBehaviourList[playerId].GetCameraLblJson());
                dictToReturn.Add("cameraPxlDistance", PlayerBehaviourList[playerId].GetCameraPxlJson());
                dictToReturn.Add("sensorsData",
                    JsonConvert.SerializeObject(PlayerBehaviourList[playerId].GetSensorsData()));
                return JsonConvert.SerializeObject(dictToReturn);
            }
        }
    }
}