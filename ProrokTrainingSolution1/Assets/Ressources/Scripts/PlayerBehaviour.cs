﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Ressources.Scripts
{
    public class PlayerBehaviour : MonoBehaviour
    {
        public bool debugMode;


        private GameObject _player;
        private Rigidbody2D _playerPhysic;

        private bool _moveUp = false;
        private bool _moveDown = false;
        private bool _moveLeft = false;
        private bool _moveRight = false;

        private bool _rotateLeft = false;
        private bool _rotateRight = false;

        private PlayerCamera _playerCam;
        private Sensors _playerSensorsData;

        private bool _debugBool = false;

        // Start is called before the first frame update
        private void Awake()
        {
            _player = gameObject;
            _playerPhysic = _player.GetComponent<Rigidbody2D>();
            _playerCam = GetComponent<PlayerCamera>();
            _playerSensorsData = GetComponent<Sensors>();
        }

        // Update is called once per frame
        private void Update()
        {
            PlayerMotion();
            if (debugMode)
            {
                DebugPlayerMotion();
            }
        }

        private void PlayerMotion()
        {
            // Compute rotation
            var rotateSpeed = float.Parse(Config.Configuration["physic"]["playerRotateSpeed"]);

            var rotation = 0f;
            if (_rotateLeft)
            {
                rotation += rotateSpeed;
            }

            if (_rotateRight)
            {
                rotation += -rotateSpeed;
            }

            // Apply rotation
            _playerPhysic.rotation += rotation * Time.deltaTime * 100;
            _playerPhysic.angularVelocity = 0;

            // Compute velocity
            var moveSpeed = float.Parse(Config.Configuration["physic"]["playerMovingSpeed"]);
            var velocity = new Vector2(0, 0);
            if (_moveUp)
            {
                velocity += (Vector2) (transform.up * moveSpeed);
            }

            if (_moveDown)
            {
                velocity += (Vector2) (-transform.up * moveSpeed);
            }

            if (_moveLeft)
            {
                velocity += (Vector2) (-transform.right * moveSpeed);
            }

            if (_moveRight)
            {
                velocity += (Vector2) (transform.right * moveSpeed);
            }

            // Apply velocity
            if (Math.Abs(velocity.magnitude) > .2f)
            {
                _playerPhysic.velocity = velocity * moveSpeed / velocity.magnitude;
            }
            else
            {
                _playerPhysic.velocity = velocity;
            }
        }

        private void DebugPlayerMotion()
        {
            _moveUp = Input.GetKey(Config.KcUp);
            _moveDown = Input.GetKey(Config.KcDown);
            _moveLeft = Input.GetKey(Config.KcLeft);
            _moveRight = Input.GetKey(Config.KcRight);

            _rotateLeft = Input.GetKey(Config.KcRotateLeft);
            _rotateRight = Input.GetKey(Config.KcRotateRight);
        }

        public void ApplyCommandMessage(Dictionary<string, string> rcvCommand)
        {
            _moveUp = rcvCommand["moveUp"] == "1";
            _moveDown = rcvCommand["moveDown"] == "1";
            _moveLeft = rcvCommand["moveLeft"] == "1";
            _moveRight = rcvCommand["moveRight"] == "1";

            _rotateLeft = rcvCommand["rotateLeft"] == "1";
            _rotateRight = rcvCommand["rotateRight"] == "1";
        }

        public string GetCameraLblJson()
        {
            return _playerCam.GetCameraLblJson();
        }

        public Dictionary<string, int> GetSensorsData()
        {
            return _playerSensorsData.GetSensorsData();
        }

        public string GetCameraPxlJson()
        {
            return _playerCam.GetCameraPxlJson();
        }

        public void RefreshCamera()
        {
            _playerCam.RefreshCam();
        }

        public void RefreshCameraAsync()
        {
            // var tStart = DateTime.Now; // DEBUG
            _playerCam.RefreshCameraLblJson();
            // Debug.Log("CAMERA PXL : " + (DateTime.Now - tStart).TotalMilliseconds);
            // tStart = DateTime.Now; // DEBUG
            _playerCam.RefreshCameraDistanceJson();
            // Debug.Log("CAMERA DISTANCE : " + (DateTime.Now - tStart).TotalMilliseconds);
        }
    }
}