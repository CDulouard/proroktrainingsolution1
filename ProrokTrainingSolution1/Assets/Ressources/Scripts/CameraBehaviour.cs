﻿using System;
using UnityEngine;

namespace Ressources.Scripts
{
    public class CameraBehaviour : MonoBehaviour
    {
        public bool lockOnTarget;
        public Transform target;


        void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
            CheckTarget();
            FollowTarget();
            CameraMotion();
        }

        private void CameraMotion()
        {
            // Compute rotation
            var rotateSpeed = float.Parse(Config.Configuration["physic"]["cameraRotateSpeed"]);
            var rotation = 0f;
            if (Input.GetKey(Config.KcRotateLeft))
            {
                rotation += rotateSpeed;
            }

            if (Input.GetKey(Config.KcRotateRight))
            {
                rotation += -rotateSpeed;
            }

            // Apply rotation
            var tmp = transform.rotation.eulerAngles;
            tmp.z += rotation;
            transform.rotation = Quaternion.Euler(tmp);

            // Compute velocity
            if (lockOnTarget) return;
            var movingSpeed = float.Parse(Config.Configuration["physic"]["cameraMovingSpeed"]);
            var velocity = new Vector2(0, 0);
            if (Input.GetKey(Config.KcUp))
            {
                velocity += (Vector2) (transform.up * movingSpeed);
            }

            if (Input.GetKey(Config.KcDown))
            {
                velocity += (Vector2) (-transform.up * movingSpeed);
            }

            if (Input.GetKey(Config.KcLeft))
            {
                velocity += (Vector2) (-transform.right * movingSpeed);
            }

            if (Input.GetKey(Config.KcRight))
            {
                velocity += (Vector2) (transform.right * movingSpeed);
            }
            
            // Apply velocity
            if (Math.Abs(velocity.magnitude) > .2f)
            {
                var transform1 = transform;
                transform1.position += (Vector3) (velocity * movingSpeed / velocity.magnitude) * Time.deltaTime;
            }
        }


        private void FollowTarget()
        {
            if (!lockOnTarget) return;
            var transform1 = transform;
            var temp = transform1.position;
            var position = target.position;
            temp.x = position.x;
            temp.y = position.y;
            transform1.position = temp;
        }

        private void CheckTarget()
        {
            if (target == null)
            {
                lockOnTarget = false;
            }
        }
    }
}